#ifndef FILE_H
#define FILE_H

#include <stdio.h>

enum io_status {
    IO_OK = 0,
    IO_EMPTY_TARGET,
    IO_NO_SUCH_FILE_OR_DIRECTORY,
};

enum io_status open_file(FILE **file, const char *name, const char *mode);
void close_file(FILE **file);

#endif
