#ifndef ROTATE_H
#define ROTATE_H
#include "bmp.h"
#include "file.h"
#include "image.h"
#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>


int rotate( char* in, char* out);

#endif
