#include "../include/bmp.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static const uint32_t TYPE = 19778;
static const uint32_t RESERVED = 0;
static const uint32_t SIZE = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIXELS_PER_METER = 2834;
static const uint32_t CLR_USED = 0;
static const uint32_t CLR_IMPORTANT = 0;
static inline uint64_t calculate_padding(uint64_t width) {
    return width % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    if(in == NULL) return READ_ERROR;
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;

    *img = image_create(header.biHeight, header.biWidth);
    if(fseek(in, header.bOffBits, SEEK_SET) != 0) return READ_ERROR;

    const size_t size_per_row = (size_t)(img->width) * sizeof(struct pixel);
    const size_t padding = calculate_padding(img->width);

    for (uint64_t i = 0; i < img->height; i++) {
        if (fread(img->data + i * (img->width), size_per_row, 1, in) != 1 ||
            fseek(in, padding, SEEK_CUR)){
            image_destroy(img);
            return READ_ERROR;
        }
            
    }

    return READ_OK;
}

static inline struct bmp_header create_bmp_header(uint64_t width, uint64_t height, uint64_t image_size) {
    return (struct bmp_header) {
        .bfType = TYPE,
        .bfSize = image_size + sizeof(struct bmp_header),
        .bfReserved = RESERVED,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage = image_size,
        .biXPelsPerMeter = PIXELS_PER_METER,
        .biYPelsPerMeter = PIXELS_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT,
    };
}

enum write_status to_bmp(FILE* out,const struct image* img) {
    if (img->width == 0 || img->height == 0 || img->data == NULL)
        return WRITE_INVALID_SOURCE;

    const size_t padding = calculate_padding(
        img->width);
    const size_t size_per_row = img->width * sizeof(struct pixel);

    const struct bmp_header header = create_bmp_header(img->width, img->height, (size_per_row + padding) * img->height);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1 ||
        fseek(out, header.bOffBits, SEEK_SET))
        return WRITE_HEADER_ERROR;

    uint8_t padding_bytes[4];
    for (size_t i = 0; i < padding; i++) padding_bytes[i] = 0;

    for (uint64_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + i * img->width, size_per_row, 1, out) != 1 ||
            fwrite(padding_bytes, padding, 1, out) != 1) {
            return WRITE_ERROR;
        }
    }
    
    return WRITE_OK;
}
