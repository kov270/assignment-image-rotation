#include "../include/rotate.h"
#include <stdio.h>



int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr,
                "Use this program with <input_file_name> and "
                "<output_file_name> arguments\n");
        return 1;
    }
    char* input_file_name = argv[1];
    char* output_file_name = argv[2];
    
    return rotate(input_file_name, output_file_name);
}
