#include "../include/image.h"

struct image image_create(uint64_t height, uint64_t width) {
    struct pixel* data = malloc(sizeof(struct pixel) * height * width);
    return (struct image){.height = height, .width = width, .data = data};
}

struct pixel image_get_pixel(const struct image* image, uint64_t x, uint64_t y) {
    return image->data[image->width * y + x];
}

void image_set_pixel(struct image* image, struct pixel pixel, uint64_t x, uint64_t y) {
    image->data[image->width * y + x] = pixel;
}

void image_destroy(struct image* image) { 
    free(image->data);
}

struct image rotate_image(struct image source) {
    struct image result = image_create(source.width, source.height);

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            image_set_pixel(&result, image_get_pixel(&source, x, y), source.height - 1 - y, x);
        }
    }
    return result;
}
