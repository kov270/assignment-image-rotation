#include "../include/file.h"

enum io_status open_file(FILE **file, const char *name, const char *mode) {
    if (!file) return IO_EMPTY_TARGET;
    *file = fopen(name, mode);
    return (*file == NULL) ? IO_NO_SUCH_FILE_OR_DIRECTORY : IO_OK;
}

void close_file(FILE **file) {
    fclose(*file);
}
